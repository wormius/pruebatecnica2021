from django.db.models import Q

from django.views.generic import View, TemplateView

from django.shortcuts import render, resolve_url, redirect, get_object_or_404

from django.core.paginator import Paginator

from .models import GLIManager

from .forms import GLIManagerForm


class GLIManagerListView(View):
    def get(self, request):

        # Filtrando por buscador
        search_filter = request.GET.get('search')

        # Filtrando por area
        area_filter = request.GET.get('area')
        if area_filter not in list(zip(*GLIManager.AREA))[0]:
            area_filter = None
           
        
        # Filtrando por estado
        try:
            status_filter = request.GET.get('status')
            if status_filter is not None:
                status_filter = int(status_filter)
                if status_filter not in list(zip(*GLIManager.STATUS))[0]:
                    status_filter = None
        except ValueError:
            status_filter = None
        

        # Resultados por pagina
        rows = request.GET.get('rows') or 5
        try:
            rows = int(rows)
        except ValueError:
            rows = 5
        except TypeError:
            rows = 5


        managers = GLIManager.objects.all()
        managers = managers.filter(Q(first_name__icontains=str(search_filter)) | Q(last_name__icontains=str(search_filter))) if search_filter is not None else managers
        if area_filter is not None: managers = managers.filter(area=area_filter)
        if status_filter is not None: managers = managers.filter(status=status_filter)

        managers = managers.order_by('-pk')
        paginator = Paginator(managers, rows)
        page_number = request.GET.get('page') or 1
        page_managers = paginator.get_page(page_number)
        
        search_filter = "" if search_filter is None else str(search_filter)
        area_filter = "" if area_filter is None else str(area_filter)
        status_filter = "" if status_filter is None else status_filter

        return render(request, 'glimanagers/list.html', 
                    context={
                        'page_managers': page_managers,
                        'nrows': rows,
                        'niof': (int(page_number)-1)*rows+1,
                        'nfof': (int(page_number)-1)*rows+len(page_managers),
                        'areaopt': GLIManager.AREA,
                        'statusopt': GLIManager.STATUS,
                        'search_filter': search_filter,
                        'area_filter': area_filter,
                        'status_filter': status_filter,
                    })

class GLIManagerAddView(View):
    def get(self, request):
        form = GLIManagerForm()
        return render(request, 'glimanagers/create.html', context={'form':form})

    def post(self, request):
        form = GLIManagerForm(request.POST, files=request.FILES)
        if (form.is_valid()):
            glimanager = form.save()
            return redirect(resolve_url('glimanagers:list'))
        else:
            return render(request, 'glimanagers/create.html', context={'form':form})


class GLIManagerDetailsView(View):
    def get(self, request, pk):
        glimanager = get_object_or_404(GLIManager, pk=pk)
        return render(request, 'glimanagers/details.html', context={'manager':glimanager})


class GLIManagerUpdateView(View):
    def get(self, request, pk):
        glimanager = get_object_or_404(GLIManager, pk=pk)
        form = GLIManagerForm(instance=glimanager)
        return render(request, 'glimanagers/update.html', context={'form':form, 'manager':glimanager})

    def post(self, request, pk):
        glimanager = get_object_or_404(GLIManager, pk=pk)
        form = GLIManagerForm(request.POST, files=request.FILES, instance=glimanager)
        if (form.is_valid()):
            glimanager = form.save()
            return redirect(resolve_url('glimanagers:success'))
        else:
            return render(request, 'glimanagers/update.html', context={'form':form, 'manager':glimanager})

class GLIManagerUpdateSuccessView(TemplateView):
    template_name = 'glimanagers/success.html'


class GLIManagerDeleteView(View):
    def post(self, request):
        glimanager = get_object_or_404(GLIManager, pk=request.POST.get('pk'))
        glimanager.delete()
        return redirect(resolve_url('glimanagers:list'))