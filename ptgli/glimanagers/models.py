from django.db import models

import uuid
from PIL import Image
from io import BytesIO
from django.core.files import File
from django.core.files.base import ContentFile

class GLIManager(models.Model):
    AREA = [
        ('a001', 'Recursos Humanos'),
        ('a002', 'Área 2'),
        ('a003', 'Área 3'),
        ('a004', 'Área 4'),
    ]
    STATUS = [
        (1, 'Activo'),
        (0, 'Inactivo'),
    ]
    display = models.ImageField(upload_to='glimanagers/', null=True, blank=True)
    first_name = models.CharField(max_length=150, default='')
    last_name = models.CharField(max_length=150, default='')
    email = models.EmailField(max_length=150, default='')
    area = models.CharField(max_length=4, choices=AREA, default=AREA[0])
    status = models.IntegerField(choices=STATUS, default=0)

    def save(self, *args, **kwargs):
        if self.pk is None and self.display:
            im = Image.open(self.display)
            source_image = im.convert('RGB')
            source_image.thumbnail((600, 600*im.size[1]/im.size[0]))
            output = BytesIO()
            source_image.save(output, format='JPEG')
            output.seek(0)
            content_file = ContentFile(output.read())
            file = File(content_file)
            random_name = f'{uuid.uuid4()}.jpeg'
            self.display.save(random_name, file, save=False)
        return super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        self.display.delete()
        return super().delete(*args, **kwargs)
