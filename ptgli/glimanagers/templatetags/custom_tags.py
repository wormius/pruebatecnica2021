from django import template
from django.template.defaultfilters import stringfilter
from django.utils.safestring import mark_safe

register = template.Library()

@register.filter
@stringfilter
def safe_email(value):
    email = '<span style="display:none;">GLI</span>{0}<span style="display:none;">GLI</span>@<span style="display:none;">GLI</span>{1}<span style="display:none;">GLI</span>'.format(*value.split('@'))        
    return mark_safe(email)
