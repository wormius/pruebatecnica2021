import django.forms as forms

from .models import GLIManager

class GLIManagerForm(forms.ModelForm):
    """Form definition for GLIManager."""

    class Meta:
        """Meta definition for GLIManagerform."""
        model = GLIManager
        fields = ('display', 'first_name', 'last_name', 'email', 'area', 'status')

    def __init__(self, *args, **kwargs):
        super(GLIManagerForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['email'].required = True
