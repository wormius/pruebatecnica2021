from django.urls import path

from . import views

app_name = 'glimanagers'

urlpatterns = [
    path('', views.GLIManagerListView.as_view(), name='list'),
    path('add/', views.GLIManagerAddView.as_view(), name='add'),
    path('<int:pk>/update/', views.GLIManagerUpdateView.as_view(), name='update'),
    path('update/success', views.GLIManagerUpdateSuccessView.as_view(), name='success'),
    path('<int:pk>/', views.GLIManagerDetailsView.as_view(), name='details'),
    path('delete/', views.GLIManagerDeleteView.as_view(), name='delete'),
]