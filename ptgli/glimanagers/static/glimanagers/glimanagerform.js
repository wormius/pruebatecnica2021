function img_dropable_set_preview(f, darea){
    let img_preview = darea.querySelector('.gli-img-dropable-preview');
    let reader = new FileReader();
    reader.readAsDataURL(f);
    reader.onloadend = function() {    
        img_preview.innerHTML = '<img src="'+reader.result+'" alt="">';
        img_preview.classList.remove('gli-img-dropable-no-preview');
    }    
}

window.addEventListener('DOMContentLoaded', (event) => {

    // Subir imagen
    var img_droped = false;
    var inputimg = document.getElementById('id_display');
    var img_dropable_area = document.querySelector('.gli-img-dropable');
    // Arrastrar y soltar
    img_dropable_area.addEventListener('dragover', function(ev){
        ev.preventDefault();
    });
    img_dropable_area.addEventListener('drop', function (ev) {
        ev.preventDefault();
        let data = ev.dataTransfer.items;
        let img_dropable_area = document.querySelector('.gli-img-dropable');

        if (data) {
            if ((data[0].kind == 'file') && (data[0].type.match('^image/'))) {
                img_droped = data[0].getAsFile();
                if (img_droped.size <= 2097152){
                    img_dropable_set_preview(img_droped, img_dropable_area);
                    inputimg.files = ev.dataTransfer.files;
                    ev.dataTransfer.items.clear();
                }
                else {
                    alert('La imagen debe pesar 2MB o menos');
                }
            }
            else {
                ev.dataTransfer.items.clear();
            }
        }
        else {
            ev.dataTransfer.clearData();
        }
    });
    // Seleccionar archivo directamente
    inputimg.addEventListener('change', function (ev){
        let data = inputimg.files;
        if (data[0].type.match('^image/')) {
            if (data[0].size <= 2097152){
                img_droped = data[0];
                img_dropable_set_preview(img_droped, img_dropable_area);
            }
            else {
                alert('La imagen debe pesar 2MB o menos');
                img_droped = false;
            }
        }
    });
    
    // Enviar formulario
    var addbutton = document.getElementById("addbutton");
    addbutton.addEventListener('click', function(){
        let addform = document.getElementById("addform");
        let isvalid = true;
        // Campos obligatorios
        let field = document.getElementById("id_first_name")
        if (field.value === ""){
            field.parentElement.parentElement.querySelector("small.gli-form-error").textContent = "Campo requerido";
            isvalid = false;
        } else field.parentElement.parentElement.querySelector("small.gli-form-error").textContent = "";
        field = document.getElementById("id_last_name")
        if (document.getElementById("id_last_name").value === ""){
            field.parentElement.parentElement.querySelector("small.gli-form-error").textContent = "Campo requerido";
            isvalid = false;
        } else field.parentElement.parentElement.querySelector("small.gli-form-error").textContent = "";
        field = document.getElementById("id_email")
        if (document.getElementById("id_email").value === ""){
            field.parentElement.parentElement.querySelector("small.gli-form-error").textContent = "Campo requerido";
            isvalid = false;
        } else field.parentElement.parentElement.querySelector("small.gli-form-error").textContent = "";
        if (isvalid) {
            addform.submit();
        }
    });
});
