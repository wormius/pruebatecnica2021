window.addEventListener('DOMContentLoaded', (event) => {
    var filters_button = document.querySelectorAll('.filters_button');
    for (const fbutton of filters_button) {
        fbutton.addEventListener('click', function() {
            var filters_form = document.getElementById('filters_form');
            if( filters_form.style.display == '') filters_form.style.display = 'block';
            else filters_form.style.display = '';
        });
    }



    var searchfilter = document.getElementById('search_by_manager');
    var searchbtn = document.getElementById('searchbutton');
    var nrow_select = document.getElementById('nrows');
    var areafilter = document.getElementById('areaselect');
    var statusfilter = document.getElementById('statusselect');
    var applyfilters = document.getElementById('applyfilters');

    searchbtn.addEventListener('click', function() {
        window.location.href = '?search='+searchfilter.value+'&rows='+nrow_select.value+'&area='+areafilter.value+'&status='+statusfilter.value;
    });

    applyfilters.addEventListener('click', function(){
        window.location.href = '?search='+searchfilter.value+'&rows='+nrow_select.value+'&area='+areafilter.value+'&status='+statusfilter.value;
    });
    
    nrow_select.addEventListener('change', function (){
        window.location.href = '?search='+searchfilter.value+'&rows='+nrow_select.value+'&area='+areafilter.value+'&status='+statusfilter.value;
    });



    // Seleccionar items
    var checks = document.querySelectorAll('.gli-select-checkbox');
    var checkall = document.getElementById('mlist_select_all');
    checkall.addEventListener( 'change', function(){
        for (const c of checks) {
            c.checked = checkall.checked;
            if (c.checked) {
                document.getElementById(c.getAttribute('data-target')).classList.add("gli-mlist-item-active");
            }
            else {
                document.getElementById(c.getAttribute('data-target')).classList.remove("gli-mlist-item-active");
            }
        }
    });
    for (const c of checks) {
        c.checked = false;
        c.addEventListener('change', function(){
            if (c.checked) {
                document.getElementById(c.getAttribute('data-target')).classList.add("gli-mlist-item-active");
            }
            else {
                document.getElementById(c.getAttribute('data-target')).classList.remove("gli-mlist-item-active");
            }
        });
    }


    // Modal
    var modal = document.querySelector('.gli-modal');
    var delete_buttons = document.querySelectorAll('.delete-manager-button');
    var input_pk_for_delete = document.getElementById('id_input_pk');
    for (const delbtn of delete_buttons) {
        // Abrir modal
        delbtn.addEventListener('click', function(){
            modal.style.display = 'block';
            pk_for_delete =  delbtn.getAttribute('data-target-pk');
            input_pk_for_delete.value = delbtn.getAttribute('data-target-pk');
            modal.querySelector('.gli-modal-delete-confirm-name').textContent = delbtn.getAttribute('data-target-name');
            modal.querySelector('.gli-modal-delete-confirm-email').innerHTML = delbtn.getAttribute('data-target-email');
            let imgname = delbtn.getAttribute('data-target-img');
            if (imgname == '') {
                modal.querySelector('.gli-modal-delete-confirm-img > img').src = '/static/glimanagers/img/nodisplay.png';
            }
            else {
                modal.querySelector('.gli-modal-delete-confirm-img > img').src = imgname;
            }            
        });
    }
    // Cerrar modal
    var delete_close_buttons = document.querySelectorAll('.gli-modal-close');
    for (const closebtn of delete_close_buttons) {
        closebtn.addEventListener('click', function(){
            modal.style.display = '';
            input_pk_for_delete.value = -1;
        });
    }
});
