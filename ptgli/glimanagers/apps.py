from django.apps import AppConfig


class GlimanagersConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'glimanagers'
